import React from 'react';
import Child from './Hijos2';

class Parent extends React.Component {
    constructor(props) {
        super(props);
        this.state = { count: 0 };

        this.outputEvent = this.outputEvent.bind(this);
    }
    outputEvent(event) {
        // the event context comes from the Child
        this.setState({ count: this.state.count + 1 });
    }

    render() {
        const variable = 5;
        return (
            <div>
                <h1>Count: { this.state.count }</h1>
                <br/>
                <Child clickHandler={this.outputEvent} />
            </div>
        );
    }
}

export default Parent;