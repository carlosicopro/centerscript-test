import React, { Component } from 'react';
import Child from "./Child";
import OtherChild from "./OtherChild";

class Parent extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            fieldVal: "",
            fieldVal2: "",
            finalstate:""
         }
    }
    onUpdate = (val) => {
        this.setState({
          fieldVal: val
        })
      };
      onUpdate2 = (val) => {
        this.setState({
          fieldVal2: val
        })
      };
      joinstate=()=>
      {
        this.setState({
          finalstate: "Campaign"+this.state.fieldVal+",ACDs"+this.state.fieldVal2
        })
      };
      
      render() {
        return (
          <div>
            <h2>Parent</h2>
            Value in Parent Component State1: {this.state.fieldVal}
            <h2>Parent</h2>
            Value in Parent Component State2: {this.state.fieldVal2}
            <br/>
            <Child onUpdate={this.onUpdate} />
            <br />
            <OtherChild onUpdate={this.onUpdate2} />
            <button onClick={this.joinstate}/>
            {this.state.finalstate}
          </div>
          
        )
      }
    }
 
export default Parent;