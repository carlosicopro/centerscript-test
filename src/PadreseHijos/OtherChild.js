import React, { Component } from 'react';
class OtherChild extends Component {

  state = { fieldVal: "" }
  update = (e) => {
    console.log(e.target.value);
    this.props.onUpdate(e.target.value);
    this.setState({fieldVal: e.target.value});
  };
    render() {
        return (
          <div>
            <input
          type="text"
          placeholder="type here"
          onChange={this.update}
          value={this.state.fieldVal}
        />
          </div>
        )
      }
    }
 
export default OtherChild;