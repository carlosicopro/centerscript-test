import React, { Component } from "react";
import {htmlexport} from "./constante"
import "./Selector.css";
class ObjectMove extends Component {
  state = {};
  constructor(props) {
    super(props);
    this.state = {
      value: 1,

      items: [
        { id: 1, Name: "jose" },
        { id: 2, Name: "omar" },
        { id: 3, Name: "thor" },
        { id: 4, Name: "ulises" },
        { id: 5, Name: "Toño" },
        { id: 6, Name: "Carlos" },
        { id: 7, Name: "Erik" }
      ]
    };

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.setState({ value: event.target.value });
  }

  focused=()=> {
    document.getElementById(this.state.value).focus();
  }

  render() {
    return (
      <div>
        {htmlexport()}
        <div>
            a donde te mueves
            <select id={0} value={this.state.value} onChange={this.handleChange}>
              {this.state.items.map(objet => (
                <option value={objet.id}>{objet.Name}</option>
              ))}
            </select>
            <button onClick={this.focused}>muevete</button>
             </div>
        <div className="GeneralDiv">
          hola
          {this.state.items.map(objet => (
              <div>
                   <p>
                 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin metus justo, consequat at augue sed, varius pellentesque ante. Vestibulum elementum dui in lacus porta facilisis. Suspendisse pulvinar tellus quis enim gravida tincidunt. Nam ultrices, sem at pellentesque ullamcorper, erat nibh vestibulum elit, non vulputate turpis massa iaculis est. Cras bibendum laoreet consectetur. Donec in posuere ipsum, vitae fermentum ligula. Mauris ut mollis nunc, et aliquam elit. Nunc bibendum lacus magna, non gravida dolor pulvinar non. Donec turpis dui, congue nec condimentum sed, lacinia in dui. Cras non facilisis justo. Nullam pellentesque iaculis dictum. In tincidunt, turpis et vehicula interdum, libero nisi finibus eros, vel condimentum nulla sapien quis libero. Mauris ut ultrices mi.

Vivamus tristique, arcu at maximus ornare, dolor massa suscipit lectus, non rhoncus orci nibh eu ante. Vestibulum faucibus ultrices purus, sit amet tempus leo. Nunc tincidunt tempor justo, vel lacinia quam faucibus sit amet. Morbi tempor nisi iaculis, commodo massa ut, laoreet ante. Praesent congue ut erat fringilla pretium. Phasellus fermentum vehicula ligula. Sed ut mi magna. Aliquam maximus laoreet turpis, vitae vulputate ligula sollicitudin id. Vivamus egestas suscipit risus vel tristique. In rutrum nisi lobortis ipsum pharetra, at posuere turpis eleifend. Mauris sit amet turpis dignissim, condimentum sapien in, lacinia tellus. Aliquam lacinia tellus ut faucibus sagittis. Nulla sagittis magna id metus vehicula, quis commodo justo commodo. Fusce id nibh mollis, dignissim ex quis, maximus urna. Aliquam sit amet fermentum ligula, vitae sodales tellus. Donec convallis libero odio.

Aliquam a erat ultrices, egestas erat a, elementum dolor. Etiam at mi sed sem consectetur ullamcorper. Sed mi enim, tincidunt sed ornare ut, venenatis vitae sapien. Curabitur pharetra a tellus ac ultricies. Donec id imperdiet dolor. Nam varius nisi at interdum egestas. Cras dignissim sollicitudin lacus a imperdiet. Aenean iaculis, risus sodales mattis dapibus, sapien elit eleifend magna, in facilisis dolor purus id metus. Donec ac tincidunt turpis.

Maecenas risus nunc, cursus at dapibus eu, placerat non augue. Donec vitae imperdiet ipsum. Etiam quis aliquam tortor. Donec odio felis, placerat quis odio non, volutpat ultrices enim. Vivamus quis malesuada sapien. Aenean ultricies vehicula porta. Curabitur luctus, orci id congue dictum, dolor mi convallis erat, vitae pharetra mauris nisl quis erat. Etiam purus ante, laoreet et elit a, tempus gravida nunc. Phasellus leo sapien, finibus id felis quis, dapibus laoreet sapien. Aliquam erat volutpat. Donec nec efficitur dui, in luctus nisi. Mauris non arcu a lacus volutpat dapibus at vitae felis. Suspendisse lobortis lacus vitae libero pharetra, non suscipit felis luctus. Mauris consectetur mauris libero, sed egestas dui pharetra id. Suspendisse pharetra sem in magna pharetra, id porta dolor auctor. Sed turpis arcu, euismod non ligula non, consequat lobortis lectus.

Nam ac ante porttitor, dapibus est sed, laoreet orci. Ut vitae volutpat nisl. Suspendisse viverra rhoncus porttitor. Donec ullamcorper arcu a finibus fringilla. Sed imperdiet molestie nisl ut interdum. Nunc hendrerit magna metus, sed vulputate tellus lobortis malesuada. Vestibulum ante ipsum, convallis egestas mi consectetur, vehicula porttitor turpis. Morbi nunc arcu, aliquet id pulvinar eget, aliquam eget justo. Curabitur ac mattis diam, quis lobortis mauris. Ut ac lectus mauris. Phasellus vehicula leo pellentesque, vestibulum nisl quis, dignissim erat. 
              </p>
            <input className="DivContent" id={objet.id} value={objet.Name}/>
            <button onClick={()=>document.getElementById(0).focus()}>return</button>
              </div>
             
          ))}
        </div>
      </div>
    );
  }
}

export default ObjectMove;
