import React, { Component } from 'react';
class Referencia extends Component {

    handleSubmit = () => {
        this.textInput.focus();
        this.textInput.value = "carlos";
      };
      handleSubmit2 = () => {
        this.elem.focus();
        this.elem.value = "jose";
      };
    
    
      render() {
        return (
          <div>
            
              
              <input type="text" ref={e => this.elem= e} />
              <input type="text" ref={e => this.textInput= e} />
              <input type="text" ref={e => this.textInput= e} />
              <input type="text" ref={e => this.textInput= e} />
              <input type="text" ref={e => this.textInput= e} />
              <input type="text" ref={e => this.textInput= e} />

              <button onClick={this.handleSubmit}>move</button>
              <button onClick={this.handleSubmit2}>move</button>
            
          </div>
        );
      }
}
 
export default Referencia;