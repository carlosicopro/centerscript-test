import React from 'react'
import reactCSS from 'reactcss'
import { ChromePicker,CirclePicker } from 'react-color'

class SketchExample extends React.Component {
  state = {
    displayColorPicker: false,
    color: '#fff'
  };

  handleClick = () => {
    this.setState({ displayColorPicker: !this.state.displayColorPicker })
  };

  handleClose = () => {
    this.setState({ displayColorPicker: false })
  };
  handleChange2 = (color) => {
    this.setState({ color: color.hex })
  };

  render() {

    const styles = reactCSS({
      'default': {
        color: {
          width: '50px',
          height: '50px',
          borderRadius: '50%',
          background: this.state.color,
        },
        swatch: {
          background: '#fff',
          borderRadius: '50px',
          border: '5px solid black',
          boxShadow: '0 0 0 1px rgba(0,0,0,.1)',
          display: 'inline-block',
          cursor: 'pointer',
        },
        popover: {
          display: 'grid',
          position: 'absolute',
          zIndex: '2',
        },
        cover: {
          position: 'fixed',
          top: '0px',
          right: '0px',
          bottom: '0px',
          left: '0px',
        },
      },
    });

    return (
      <div>
        <div style={{display: 'grid'}}>
          <div style={{backgroundColor: this.state.color, color:'white', border: '2px solid'+this.state.color, width:'50px'}}>:)</div><input placeholder="example" style={{backgroundColor: this.state.color, color:'white', border: '2px solid'+this.state.color,width:'100px'}}></input>
          <div style={{color: this.state.color, backgroundColor:'white', border: '2px solid'+this.state.color, width:'50px'}}>:)</div><input placeholder="example" style={{color: this.state.color, backgroundColor:'white', border: '2px solid'+this.state.color,width:'100px'}}></input>
        </div>
        <CirclePicker onChange={ this.handleChange2 } colors={['#D9E3F0', '#F47373', '#697689', '#37D67A', '#2CCCE4', '#555555', '#dce775', '#ff8a65', '#ba68c8']}/>
        <div style={ styles.swatch } onClick={ this.handleClick }>
          <div style={ styles.color } />
        </div>
        { this.state.displayColorPicker ? <div style={ styles.popover }>
          <div style={ styles.cover } onClick={ this.handleClose }/>
          <ChromePicker color={ this.state.color } onChange={ this.handleChange2 } />
        </div> : null }

      </div>
    )
  }
}

export default SketchExample