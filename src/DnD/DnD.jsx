
import 'react-grid-layout/css/styles.css';
import 'react-resizable/css/styles.css';
import React from "react";
import GridLayout from 'react-grid-layout';

import '../DnD/DnD.css';

class MyFirstGrid extends React.Component {
  render() {
    // layout is an array of objects, see the demo for more complete usage;
    return (
      <div id="GridLayout">
        <GridLayout className="layout" cols={12} rowHeight={30} width={980} preventCollision={true} verticalCompact= {false} maxRows={14}>
          <div className="Contan" key="a" data-grid={{ x: 0, y: 0, w: 1, h: 1 }}></div>
          <div className="Contan" key="b" data-grid={{ x: 1, y: 0, w: 1, h: 1 }}></div>
          <div className="Contan" key="c" data-grid={{ x: 4, y: 0, w: 1, h: 2 }}></div>
        </GridLayout>

      </div>


    )
  }
}
export default MyFirstGrid;