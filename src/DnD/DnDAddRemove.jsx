import React from "react";
import { WidthProvider, Responsive } from "react-grid-layout";
import _ from "lodash";

import 'react-grid-layout/css/styles.css';
import 'react-resizable/css/styles.css';
import '../DnD/DnD.css';
const ResponsiveReactGridLayout = WidthProvider(Responsive);

/**
 * This layout demonstrates how to use a grid with a dynamic number of elements.
 */
class AddRemoveLayout extends React.PureComponent {
    static defaultProps = {
        className: "layout",
        cols: { lg: 12, md: 12, sm: 12, xs: 12, xxs: 12 },
        rowHeight: 50,
        onLayoutChange: function () { },
        verticalCompact: false,
        preventCollision: true
    };

    constructor(props) {
        super(props);

        this.state = {
            items: [].map(function (i, key, list) {
                return {};
            }),
            newCounter: 0
        };

        this.onAddItem = this.onAddItem.bind(this);
        this.onBreakpointChange = this.onBreakpointChange.bind(this);
    }

    createElement(el) {
        const removeStyle = {
            position: "absolute",
            right: "2px",
            top: 0,
            cursor: "pointer",

        };
        const i = el.add ? "+" : el.i;
        return (
            <div key={i} data-grid={el}>
                {el.add ? (
                    <span
                        className="add text"
                        onClick={this.onAddItem}
                        title="You can add an item by clicking here, too."
                    >
                        Add +
          </span>
                ) : (
                        <span className="text">{i}</span>
                    )}
                <span
                    className="remove"
                    style={removeStyle}
                    onClick={this.onRemoveItem.bind(this, i)}
                >
                    x
        </span>
            </div>
        );
    }

    onAddItem() {
        /*eslint no-console: 0*/
        console.log("adding", "n" + this.state.newCounter);
        this.setState({
            // Add a new item. It must have a unique key!
            items: this.state.items.concat({
                i: "Element New°" + this.state.newCounter,
                // x: (this.state.items.length * 2) % (this.state.cols || 12),
                //y: Infinity, // puts it at the bottom
                x:1,
                y:1,
                w: 1,
                h: 1
            }),
            // Increment the counter to ensure key is always unique.
            newCounter: this.state.newCounter + 1
        });
    }

    // We're using the cols coming back from this to calculate where to add new items.
    onBreakpointChange(breakpoint, cols) {
        this.setState({
            breakpoint: breakpoint,
            cols: cols
        });
    }

    onLayoutChange(layout) {
        this.props.onLayoutChange(layout);
        this.setState({ layout: layout });
    }

    onRemoveItem(i) {
        console.log("removing", i);
        this.setState({ items: _.reject(this.state.items, { i: i }) });
    }

    render() {
        return (
            <div className="row">
                <div className="column1">
                    <div className="BoxBtton">
                        <div className="bttn">
                        <button onClick={this.onAddItem}>Add Item 1</button>
                        </div>
                        <div className="bttn">
                        <button onClick={this.onAddItem}>Add Item 2</button>
                        </div>
                        <div className="bttn">
                        <button onClick={this.onAddItem}>Add Item 3</button>
                        </div>
                        <div className="bttn">
                        <button onClick={this.onAddItem}>Add Item 4</button>
                        </div>
                    </div>
                    </div>
                <div className="column2">
                    <ResponsiveReactGridLayout
                        onLayoutChange={this.onLayoutChange}
                        onBreakpointChange={this.onBreakpointChange}
                        {...this.props}
                    >
                        {_.map(this.state.items, el => this.createElement(el))}
                    </ResponsiveReactGridLayout>
                </div>

            </div>
        );
    }
}
export default AddRemoveLayout;