import React from "react";
import { WidthProvider, Responsive } from "react-grid-layout";

import "react-grid-layout/css/styles.css";
import "react-resizable/css/styles.css";
import "../DnD/DnD.css";
const ResponsiveReactGridLayout = WidthProvider(Responsive);

/**
 * This layout demonstrates how to sync multiple responsive layouts to localstorage.
 */
class ResponsiveLocalStorageLayout extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      layouts: {}
    };
  }

  resetLayout() {
    this.setState({ layouts: {} });
    console.log("Reset layout{{}}");
  }

  onLayoutChange(layout, layouts) {
    this.setState({ layouts });
  }
  saveTemplate(layouts){
    saveToLS("temp",layouts);
  }
  drawTemplate()
  {
    this.setState({layouts: JSON.parse(JSON.stringify(getFromLS("temp") || {}))})
  }

  render() {
    return (
      <div>
        <button onClick={() => this.resetLayout()}>Reset Layout</button>
        <button onClick={() => this.saveTemplate(this.state.layouts)}>save tempalte</button>
        <button onClick={() => this.drawTemplate()}>draw template</button>
        <ResponsiveReactGridLayout
          className="layout"
          cols={{ lg: 12, md: 10, sm: 6, xs: 4, xxs: 2 }}
          rowHeight={30}
          layouts={this.state.layouts}
          preventCollision = {true}
          compactType= {"Horizontal"}
          onLayoutChange={(layout, layouts) =>
            this.onLayoutChange(layout, layouts)
          }
        >
          <div className="Contan" key="1" data-grid={{ w: 2, h: 3, x: 0, y: 0, minW: 2, minH: 3 }}>
            <span className="text"><label text="hi hi"></label></span>
          </div>
          <div className="Contan"  key="2" data-grid={{ w: 2, h: 3, x: 2, y: 0, minW: 2, minH: 3 }}>
            <span className="text">2</span>
          </div>
          <div className="Contan" key="3" data-grid={{ w: 2, h: 3, x: 4, y: 0, minW: 2, minH: 3 }}>
            <span className="text">3</span>
          </div>
          <div className="Contan" key="4" data-grid={{ w: 2, h: 3, x: 6, y: 0, minW: 2, minH: 3 }}>
            <span className="text">4</span>
          </div>
          <div className="Contan" key="5" data-grid={{ w: 2, h: 3, x: 8, y: 0, minW: 2, minH: 3 }}>
            <span className="text">5</span>
          </div>
        </ResponsiveReactGridLayout>
      </div>
    );
  }
}


function getFromLS(key) {
  let ls = {};
  if (global.localStorage) {
    try {
      ls = JSON.parse(global.localStorage.getItem("rgl-8")) || {};
      console.log("Drawtempalte"+ global.localStorage.getItem("rgl-8"));
    } catch (e) {
      /*Ignore*/
    }
  }
  return ls[key];
}

function saveToLS(key, value) {
  if (global.localStorage) {
    global.localStorage.setItem(
      "rgl-8",
      JSON.stringify({
        [key]: value
      })
    );
    
    console.log("Savetempalte"+ global.localStorage.getItem("rgl-8"));
  }
}
export default ResponsiveLocalStorageLayout;