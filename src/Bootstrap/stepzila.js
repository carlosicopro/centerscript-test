import React, { Component } from 'react';
import StepZilla from "react-stepzilla";

class Stepzilla extends Component {
    render() 
    { const steps =
        [
            {name: 'Step 1', component: <div>1</div>},
            {name: 'Step 2', component: <div>2</div>},
            {name: 'Step 3', component: <div>3</div>},
            {name: 'Step 4', component: <div>4</div>},
            {name: 'Step 5', component: <div>5</div>}
        ]
        return ( <div className='step-progress'>
        <StepZilla steps={steps} stepsNavigation={false} prevBtnOnLastStep={false} startAtStep={1} />
    </div>);
    }
}
 
export default Stepzilla;