import React from "react";
import "./SideMenu.css";

const NavButton = ({onClick,show2}) => 
  <button style={{ position: "absolute", marginTop:  "200px", marginLeft: show2 ? "270px":"0px", width: "20px", height:"200px"}} onClick={onClick}>
    <div id="nav-icon1">
       <span>|</span>
       <span>|</span>
       <span>|</span>
    </div>
  </button>

const Dropdown = ({show}) => 
<div style={{visibility: show ? "visible" : "hidden", backgroundColor: "lightpink", position: "absolute", height: "100%", width: "20%"}}>
</div>

export default class Parent extends React.Component {
  state = {
    dropdownVisible: false,
  };

  // toggles the dropdown each time it is called
  toggleDropdown = () => this.setState(state => ({
    dropdownVisible: !state.dropdownVisible,
  }));

  render() {
    return (
      <div className="left col-xs-12 col-md-6">
        <Dropdown show={this.state.dropdownVisible}>
            
        <h1  className="hola">hola</h1>
        </Dropdown>


        <NavButton show2={this.state.dropdownVisible} onClick={this.toggleDropdown} />
        {this.props.children}
      </div>
    );
  }
}
 