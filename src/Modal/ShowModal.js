import React, { Component } from 'react';
import Modals from './Modals';

class ShowModal extends Component {
    render() { 
        return ( 
        <div>
            
          <Modals
            title={"Este es el titulo"}
            body={"Este es el contenido"}
            footer={"Este es el pie de pagina"}
            Type = {false}
            Show = {true}
          />
        </div> );
    }
}
 
export default ShowModal;