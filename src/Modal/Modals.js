import React, { Component } from 'react';
import "./Modals.css";

class ModalGeneric extends Component {
    constructor(props) {
        super(props);
        this.state = { 
          Window : this.props.Type,
          Show : this.props.Show
         }
    }

    toggleShow = () =>
    this.setState(state => ({
      Show: !state.Show
    }));
    toggleSize = () =>
    this.setState(state => ({
      Window: !state.Window
    }));

    
    render() {
      const ModalGen =({Window, Show}) =>(

        <div className='popup' onKeyPress={this.handleKeyDown}  
         style={{
          visibility: Show ? "visible" : "hidden",
          width: Window ? "100%" : "25%" ,
          height: Window ? "100%" : "25%"
        }}>
            <div className='popup_title'
            style ={{
              height: Window ? "5%" : "15%",
              marginTop:  "-15px"
            }}
            >
            <button className="exitBttn" onClick={this.toggleShow}>x</button>
              <h2>{this.props.title}</h2>
            </div>
            <div className='popup_content'
            style ={{
              height: Window ? "88%" : "65%"
            }}
            >
              {this.props.body}
            </div>
            <div className='popup_footer'
            style ={{
              height: Window ? "5%" : "15%",
              visibility: Show ? "visible" : "hidden"
            }}
            >
            {this.props.footer}
            
            </div>
          </div>

      );
      
        return ( 
          <div>
            <button onClick={this.toggleShow} >show</button>
            <button onClick={this.toggleSize} >Size</button>
            <ModalGen Window={this.state.Window} Show={this.state.Show} buttonVisibility={this.state.ButonVis} ></ModalGen>
          
          </div>
          
         );
    }
}
 
export default ModalGeneric;