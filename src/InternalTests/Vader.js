import React, { Component } from 'react';
import Luke from "./Luke";

class Vader extends Component {
    state = {
        items: [
          { id: 1, Name: "carlos", Text:"hola hola" },
          { id: 2, Name: "Jose" },
          { id: 3, Name: "Erik" }
        ]
      };
    
    UpdatedeHijo=(name,text)=>{
        this.state.items[0].Name = name;
        this.state.items[0].Text = text;
    }
    render() { 
        return ( 
            <div>
                id:{this.state.items[0].id} name:{this.state.items[0].Name} Text:{this.state.items[0].Text}
                <Luke Name={this.state.items[0].Name} Text={this.state.items[0].Text} Actualiza={this.UpdatedeHijo}></Luke>
            </div>
         );
    }
}
 
export default Vader;