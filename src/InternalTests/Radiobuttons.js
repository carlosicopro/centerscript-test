import React, { Component } from 'react';
import {RadioGroup, Radio} from "react-radio-group";

class Radios extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Cam: [
        { id: 1, Name: "carlos" },
        { id: 2, Name: "Jose" },
        { id: 3, Name: "Erik" }
      ]
    };
  }

  uncheckbuttons = () => {
    var doc = document.getElementsByName("fruit");
    for (let i = 0; i < doc.length; i++) {
      if (doc[i].checked) {
        doc[i].checked = false;
      }
    }
  };
  render() {
    return (
      <div>
        {this.state.selectedValue}
        <RadioGroup
          name="fruit"
          selectedValue={this.state.selectedValue}
          onChange={this.handleChange}
        >
          {this.state.Cam.map(cam => (
            <label>
              <Radio
                value={cam.id}
                id={"C" + cam.id}
                onDoubleClick={this.uncheckbuttons}
              />
              {cam.Name}
            </label>
          ))}
        </RadioGroup>
      </div>
    );
  }
}
 
export  default Radios;